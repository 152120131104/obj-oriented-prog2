﻿using System;
using System.Collections.Generic;
using System.Text;

namespace oopth5
{
    public class Products
    {
        public string name { get; set; }
        public double price { get; set; }
        public string description { get; set; }
        public string fileName { get; set; }
        public string path { get; set; }

    }
}
