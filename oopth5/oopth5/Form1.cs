﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace oopth5
{
    public partial class Form1 : Form
    {

        private List<Products> products = new List<Products>();

        string sourcePath ="";
        string targetPath = "";
        string destFile = "";
        public Form1()
        {
            InitializeComponent();

            products.Add(new Products
            {
                name = "Deterjan",
                price = 5.5,
                description = "200 ml'lik Pril Deterjan",
                fileName = "pril.png",



            });

            products.Add(new Products
            {
                name = "Cerez",
                price = 7,
                description = "Karısık cerez",
                fileName = "cerez.jpg",



            });

            products.Add(new Products
            {
                name = "Cikolata",
                price = 5.5,
                description = "Antep fıstıklı çikolata.",
                fileName = "cikolata.jpg",



            });
            products.Add(new Products
            {
                name = "Kola",
                price = 4,
                description = "2.5 lt Coca Cola",
                fileName = "cola.png",



            });
            products.Add(new Products
            {
                name = "Doritos",
                price = 9,
                description = "Taco Doritos",
                fileName = "doritos.jpg",



            });
            products.Add(new Products
            {
                name = "Ice Tea",
                price = 4,
                description = "Seftalili icetea",
                fileName = "icetea.jpg",



            });
            products.Add(new Products
            {
                name = "Cocopops",
                price = 12,
                description = "Cikolatalı corn flakes",
                fileName = "pops.jpg",



            });
            products.Add(new Products
            {
                name = "Ruffles",
                price = 6,
                description = "Peynirli ve soğanlı cips",
                fileName = "ruffles.jpg",



            });
            products.Add(new Products
            {
                name = "Sprite",
                price = 7,
                description = "Gazoz",
                fileName = "sprite.jpg",



            });
            products.Add(new Products
            {
                name = "Sut",
                price = 3,
                description = "Süt",
                fileName = "sut.jpg",



            });



            foreach (Products prod in products)
            {
                prod.path = Path.Combine(Environment.CurrentDirectory, @"Data\", prod.fileName);
                listBoxProducts.Items.Add(prod.name);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        Point lastPoint;
        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Left += e.X - lastPoint.X;
                Top += e.Y - lastPoint.Y;
            }
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

        private void btnAddProduct_Click(object sender, EventArgs e)
        {

            if(txtName.Text!="" && txtPrice.Text!="" && txtDesc.Text!="" && txtFileName.Text!="")
            {
                products.Add(new Products
                {
                    name = txtName.Text,
                    price = double.Parse(txtPrice.Text),
                    description = txtDesc.Text,
                    fileName = txtFileName.Text
                });

                try
                {
                    Directory.CreateDirectory(targetPath);
                    File.Copy(sourcePath, destFile, true);
                    Products prod = products.SingleOrDefault(c => c.name == txtName.Text);
                    prod.path = Path.Combine(Environment.CurrentDirectory, @"Data\", prod.fileName);
                    listBoxProducts.Items.Add(prod.name);
                }
                catch(Exception ex)
                {
                    MessageBox.Show("This image is used for another product! Please choose another one.");
                }

                
                clearText();
            }


            else
            {
                MessageBox.Show("There are some blanks that are need to be fillen. ");
            }
        }

        private void clearText()
        {
            txtFileName.Clear();
            txtName.Clear();
            txtDesc.Clear();
            txtPrice.Clear();
        }

        private void listBoxProducts_SelectedIndexChanged(object sender, EventArgs e)
        {
            panelinformation.Visible = true;
            Products prod = products.SingleOrDefault(c => c.name == listBoxProducts.GetItemText(listBoxProducts.SelectedItem));
            showDescripton(prod);

        }

        public void showDescripton(Products prod)
        {
            if(prod!=null)
            {
                labelProductName.Text = prod.name;
                labelProductPrice.Text = Convert.ToString(prod.price) + " TL";
                labelProductDesc.Text = prod.description;

                pictureBoxProduct.Image = Image.FromFile(prod.path);
                pictureBoxProduct.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            DialogResult dr = ofd.ShowDialog();
            
            
            if (dr == DialogResult.OK)
            {
                txtFileName.Text = Path.GetFileName(ofd.FileName);
                sourcePath = ofd.FileName;
                targetPath = Path.Combine(Environment.CurrentDirectory, @"Data");
                destFile = Path.Combine(targetPath, Path.GetFileName(ofd.FileName));
            }

        }

        private void btnDeleteProduct_Click(object sender, EventArgs e)
        {
            if (listBoxProducts.SelectedItems.Count > 0)
            {
                listBoxProducts.Items.Remove(listBoxProducts.SelectedItems[0]);
                Products prod = products.SingleOrDefault(c => c.name == listBoxProducts.GetItemText(listBoxProducts.SelectedItem));
                if(prod!=null)
                {
                    products.Remove(prod);
                }
            }
            else
            {
                MessageBox.Show("You need to select product that is wanted to remove.");
            }

            panelinformation.Visible = false;

        }

        private void btnUpdateProduct_Click(object sender, EventArgs e)
        {
            panelUpdate.Visible = true;
            Products prod = products.SingleOrDefault(c => c.name == listBoxProducts.GetItemText(listBoxProducts.SelectedItem));

            txtNewName.Text = prod.name;
            txtNewPrice.Text = Convert.ToString(prod.price);
            txtNewDesc.Text = prod.description;
            txtNewFileName.Text = prod.fileName;

        }

        private void btnAcceptChanges_Click(object sender, EventArgs e)
        {
            Products prod = products.SingleOrDefault(c => c.name == listBoxProducts.GetItemText(listBoxProducts.SelectedItem));
            
            if (txtNewName.Text != "" && txtNewPrice.Text != "" && txtNewDesc.Text != "" && txtNewFileName.Text != "")
            {

                try
                {
                    Directory.CreateDirectory(targetPath);
                    File.Copy(sourcePath, destFile, true);

                    prod.name = txtNewName.Text;
                    prod.price = double.Parse(txtNewPrice.Text);
                    prod.fileName = txtNewFileName.Text;
                    prod.path = Path.Combine(Environment.CurrentDirectory, @"Data\", prod.fileName);
                    listBoxProducts.Items[listBoxProducts.SelectedIndex] = prod.name;



                    txtNewName.Text = "";
                    txtNewPrice.Text = "";
                    txtNewDesc.Text = "";
                    txtNewFileName.Text = "";
                    
                    panelUpdate.Visible = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("This image is used for another product! Please choose another one.");
                }

            }


            else
            {
                MessageBox.Show("There are some blanks that are need to be fillen. ");
            }

        }

        private void btnBrowseNew_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            DialogResult dr = ofd.ShowDialog();
            if (dr == DialogResult.OK)
            {
                txtNewFileName.Text = Path.GetFileName(ofd.FileName);
                sourcePath = ofd.FileName;
                targetPath = Path.Combine(Environment.CurrentDirectory, @"Data");
                destFile = Path.Combine(targetPath, Path.GetFileName(ofd.FileName));
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            panelUpdate.Visible = false;
        }
    }

}
